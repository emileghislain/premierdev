<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;

class organizationController extends Controller
{
    public function index(){
        $data = Organization::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
