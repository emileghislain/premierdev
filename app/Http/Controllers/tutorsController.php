<?php

namespace App\Http\Controllers;

use App\Tutor;
use Illuminate\Http\Request;

class tutorsController extends Controller
{
    public function index(){
        $data = Tutor::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
