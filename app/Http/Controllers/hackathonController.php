<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Hackathon;

use Illuminate\Http\Request;

class hackathonController extends Controller
{
    public function index(){
        $data = Hackathon::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
