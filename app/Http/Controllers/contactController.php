<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
use App\Contact;


class contactController extends Controller
{
    public function index(){
        $data = Contact::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function contact(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'email'=>'required|email',
            'message'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors(),
            ], 400);
        }

        $data = [
            'username_from' => $request->name,
            'email_from' => $request->email,
            'username_to' => env('APP_NAME'),
            'email_to' => 'contact@gmail.com',
            'text' => $request->message,
            'thank' => 'Merci de nous faire confiance !',
            'subject' => 'Mail de Contact'
        ];

        //Mail::to(env('MAIL_FROM_ADDRESS'), env('APP_NAME'))->send(new Notification($data));

        DB::table('contacts')->insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'message'=>$request->message,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        return response()->json([
            'message'=>'commentaire enregistré'
        ], 200);
    }
}
