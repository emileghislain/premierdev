<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Value;

class valueController extends Controller
{
    public function index(){
        $data = Value::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
