<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Categorie;
use App\Publication;

class categorieController extends Controller
{
    public function index(){
        $data = Categorie::all();
        $data = $data->sortBy('categorie');
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function categories($id){
        $categ = DB::table('categories')->where('id', $id)->first();
        $data = Publication::all();
        $name = $categ->categorie;
        $data = $data->filter(function($item) use($name){
            $tab = json_decode($item->categorie);
            if(in_array($name, $tab)){
                return $item;
            }
        });
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function nameCat($id){
        $categ = DB::table('categories')->where('id', $id)->first();
        
        return response()->json([
            'data'=>$categ
        ], 200);
    }
}
