<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;

class teamController extends Controller
{
    public function index(){
        $data = Team::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
