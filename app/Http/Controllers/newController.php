<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class newController extends Controller
{
    public function index(){
        $data = News::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
