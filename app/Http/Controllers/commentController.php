<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;
use App\Comment;


class commentController extends Controller
{
    public function index(){
        $data = Comment::all();
        $data = $data->sortBy('id', SORT_REGULAR, true);
        $data = $data->values()->all();
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function show($id){
        $data = DB::table('comments')->where('blog_id', $id)->get();
        $data = $data->sortBy('id', SORT_REGULAR, true);
        $data = $data->values()->all();
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function comment(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'email'=>'required|email',
            'message'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'message'=>$validator->errors()
            ], 400);
        }

        DB::table('comments')->insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'message'=>$request->message,
            'blog_id'=>$request->blog_id,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        return response()->json([
            'message'=>'commentaire enregistré'
        ], 200);
    }
}
