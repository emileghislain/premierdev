<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Question;

class questionController extends Controller
{
    public function index(){
        $data = Question::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
