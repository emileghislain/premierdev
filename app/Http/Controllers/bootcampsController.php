<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Bootcamp;

use Illuminate\Http\Request;

class bootcampsController extends Controller
{
    public function index(){
        $data = Bootcamp::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
