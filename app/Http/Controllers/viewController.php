<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\View;
use Carbon\Carbon;

class viewController extends Controller
{
    public function index(){
        $data = Views::all();
        return response()->json([
            'message'=>$data
        ], 200);
    }


    public function show(Request $request){
        $validator = Validator::make($request->all(), [
            'blog_id'=>'required',
            'user'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'message'=>$validator->errors()
            ], 400);
        }

        $like = DB::table('likes')->where('id', $request->id)->where('user', $request->user)->first();
        return response()->json([
            'message'=>$like
        ], 200);
    }

    public function addView(Request $request){
        $validator = Validator::make($request->all(), [
            'blog_id'=>'required',
            'user'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'message'=>$validator->errors()
            ], 400);
        }

        $user = DB::table('views')->where('blog_id', $request->blog_id)->where('user', $request->user)->first();

        if($user){
            $date = strtotime(now()) - strtotime($user->updated_at);
            if($date > 86400){
                $view = $user->view;
                DB::table('views')->where('blog_id', $request->blog_id)->where('user', $request->user)->update([
                    'view'=> $view+1,
                    'updated_at'=>now()
                ]);
            }
        
        }else{
            DB::table('views')->insert([
                'view'=>1,
                'blog_id'=>$request->blog_id,
                'user'=>$request->user,
                'created_at'=>now(),
                'updated_at'=>now()
            ]);
        }

        $all_like = DB::table('views')->where('blog_id', $request->blog_id)->get();
        DB::table('publications')->where('id', $request->blog_id)->update([
            'view'=>$all_like->count()
        ]);

        
    }

    public function allView(Request $request){
        $validator = Validator::make($request->all(), [
            'blog_id'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'data'=>$validator->errors()
            ], 400);
        }

        $user = DB::table('views')->where('blog_id', $request->blog_id)->get();

        DB::table('publications')->where('id', $request->blog_id)->update([
            'view'=>$user->count()
        ]);

        return response()->json([
            'data'=> $user->count()
        ], 200);
    }
}