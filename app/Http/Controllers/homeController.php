<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Home;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function index(){
        $data = Home::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
