<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Publication;

class publicationController extends Controller
{
    public function index(){
        $data = Publication::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function show($id){
        $data = DB::table('publications')->where('id', $id)->first();
        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function popular(){
        $data = Publication::all();
        $data = $data->sortBy('view', SORT_REGULAR, true);
        $data = $data->values()->all();
        $data = array_slice($data, 0, 6);

        return response()->json([
            'data'=>$data
        ], 200);
    }

    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'name'=>'required'
        ]);
        if($validator->fails){
            return response()->json([
                'message'=>$validator->errors
            ], 400);
        }
        $blogs = Publication::where('name', 'LIKE', '%' . $request->name . '%')->get();

        return response()->json([
            'message'=>$blogs
        ], 200);
    }
}