<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Like;

class likeController extends Controller
{
    public function index(){
        $data = Likes::all();
        return response()->json([
            'message'=>$data
        ], 200);
    }


    public function show(Request $request){
        $validator = Validator::make($request->all(), [
            'user'=>'required',
            'blog_id'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'message' => $validator->errors(),
            ], 400);
        }

        $like_res = DB::table('likes')->where('blog_id', $request->blog_id)->where('user', $request->user)->first();

        if($like_res->like === '0'){
            return response([
                'data'=>false
            ], 200);
        }else{
            return response([
                'data'=>true
            ], 200);
        }
    }

    public function addLike(Request $request){
        $validator = Validator::make($request->all(), [
            'blog_id'=>'required',
            'user'=>'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'message'=>$validator->errors()
            ], 400);
        }

        $user = DB::table('likes')->where('blog_id', $request->blog_id)->where('user', $request->user)->first();

        if($user){
            $like = $user->like;
            DB::table('likes')->where('blog_id', $request->blog_id)->where('user', $request->user)->update([
                'like'=> !$like,
                'updated_at'=>now()
            ]);
        }else{
            DB::table('likes')->insert([
                'like'=>$request->like,
                'blog_id'=>$request->blog_id,
                'user'=>$request->user,
                'updated_at'=>now(),
                'created_at'=>now()
            ]);
        }

        $all_like = DB::table('likes')->where('blog_id', $request->blog_id)->where('like', '1')->get();
        DB::table('publications')->where('id', $request->blog_id)->update([
            'like'=>$all_like->count()
        ]);

        $like_res = DB::table('likes')->where('blog_id', $request->blog_id)->where('user', $request->user)->first();

        if($like_res->like === '0'){
            return response([
                'data'=>false
            ], 200);
        }else{
            return response([
                'data'=>true
            ], 200);
        }
        
    }

    public function allLike(Request $request){
        $validator = Validator::make($request->all(), [
            'blog_id'=>'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'data'=>$validator->errors()
            ], 400);
        }

        $user = DB::table('likes')->where('blog_id', $request->blog_id)->where('like', '1')->get();

        DB::table('publications')->where('id', $request->blog_id)->update([
            'like'=>$user->count()
        ]);

        return response()->json([
            'data'=> $user->count()
        ], 200);
    }
}