<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Teaching;

class teachingController extends Controller
{
    public function index(){
        $data = Teaching::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}