<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sponsor;

class sponsorController extends Controller
{
    public function index(){
        $data = Sponsor::all();
        return response()->json([
            'data'=>$data
        ], 200);
    }
}
