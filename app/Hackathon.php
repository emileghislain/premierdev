<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Hackathon extends Model
{
    use HasFactory;

    protected $table = 'hackathons';
    protected $fillable = [
        'image',
        'title',
        'subtitle',
    ];
}
