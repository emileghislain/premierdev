<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bootcamp extends Model
{
    use HasFactory;

    protected $table = 'bootcamps';
    protected $fillable = [
        'image',
        'name',
        'content'
    ];
}