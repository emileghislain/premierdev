<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Publication extends Model
{
    protected $table = 'publications';
    protected $fillable = [
        'logo',
        'name',
        'summary',
        'title',
        'subtitle',
        'view',
        'like',
        'images',
        'categorie',
    ];
}