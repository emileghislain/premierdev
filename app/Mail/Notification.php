<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Notification extends Mailable
{
    use Queueable, SerializesModels;
    //public $data ;

    public function __construct($data)
    {
        $this->data = $data;

    }

    public function build()
    {
        return $this->view('contact', ['text'=>$this->data['text'], 'thank'=>$this->data['thank']])
                    ->from($this->data['email_from'], $this->data['username_from'])
                    ->subject($this->data['subject'])
                    ->with([ 'test_message' => $this->data['text'] ]);
    }
}
