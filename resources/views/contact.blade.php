<!DOCTYPE html>
<meta charset="utf-8">
<meta name="viewport" content="with=device-with, initial-scale=1">
<style>
    .content{
        display: flex;
        flex-direction: column;
        align-items: center;
    }
    img{
        display: flex;
        height : 50vh;
    }
</style>
<body>
<div class="content">
    <div>{{$text}}</div>
    <div>{{$thank}}</div>
</div>
</body>
</html>
