<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//header("Access-Control-Allow-origin: *");
header("Access-Control-Allow-Headers: *");

//obtenir sponsor et publication home
Route::get('home', 'App\Http\Controllers\homeController@index');
Route::get('hackathon', 'App\Http\Controllers\hackathonController@index');
Route::get('teams', 'App\Http\Controllers\teamController@index');
Route::get('student', 'App\Http\Controllers\studentController@index');
Route::get('questions', 'App\Http\Controllers\questionController@index');
Route::get('values', 'App\Http\Controllers\valueController@index');
Route::get('news', 'App\Http\Controllers\newController@index');
Route::get('testimonies', 'App\Http\Controllers\studentController@index');
Route::get('sponsors', 'App\Http\Controllers\sponsorController@index');
Route::get('organisations', 'App\Http\Controllers\organizationController@index');
Route::post('comment', 'App\Http\Controllers\commentController@comment');
Route::get('all/comment/{id}', 'App\Http\Controllers\commentController@show');
Route::post('contact', 'App\Http\Controllers\contactController@contact');

//obtenir bootcamp
Route::get('bootcamp', 'App\Http\Controllers\bootcampsController@index');
Route::get('tutors', 'App\Http\Controllers\tutorsController@index');
Route::get('teaching', 'App\Http\Controllers\teachingController@index');
//Route::get('comment', 'App\Http\Controllers\commentController@index');
Route::post('post', 'App\Http\Controllers\commentController@comment');

//pour le blog
Route::get('publications', 'App\Http\Controllers\publicationController@index');
Route::get('blog/{id}', 'App\Http\Controllers\publicationController@show');
Route::get('popular', 'App\Http\Controllers\publicationController@popular');
Route::get('categories', 'App\Http\Controllers\categorieController@index');
Route::get('categories/data/{id}', 'App\Http\Controllers\categorieController@categories');
Route::get('categories/{id}', 'App\Http\Controllers\categorieController@nameCat');

//pour le rechercher sur un blog
Route::post('search/publication', 'App\Http\Controllers\publicationController@search');

//get Like
Route::post('show/like', 'App\Http\Controllers\likeController@show');
//get all like
Route::post('all/like', 'App\Http\Controllers\likeController@allLike');
//add like
Route::post('add/like', 'App\Http\Controllers\likeController@addLike');


//get View
Route::get('show/view', 'App\Http\Controllers\viewController@show');
//get all View
Route::post('all/view', 'App\Http\Controllers\viewController@allView');
//add View
Route::post('add/view', 'App\Http\Controllers\viewController@addView');



Route::get('search', 'App\Http\Controllers\publicationController@search');


Route::get('organization', 'App\Http\Controllers\organizationController@index');



Route::get('values', 'App\Http\Controllers\valueController@index');
Route::get('organization', 'App\Http\Controllers\studentController@index');